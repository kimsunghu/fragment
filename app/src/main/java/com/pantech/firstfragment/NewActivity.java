package com.pantech.firstfragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.pantech.firstfragment.util.FileEx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Created by user on 2017-03-15.
 */

public class NewActivity extends Activity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    Intent revIntent;
    ToggleButton toggleButton;
    Button fileRead;
    Button fileWrite;
    TextView resultView;
    Intent intent;
    String fileName = "myfile";
    String string = "Hello World!";
    FileOutputStream outputStream;
    EditText editText;
    CheckBox privateChk;
    CheckBox publicChk;
    CheckBox sdcardChk;
    CheckBox usageStorage;
    final String TAG = "NewActtvity";
    File file;
    Switch aSwitch;

    FileEx fileEx;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_activity);


        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        fileRead = (Button) findViewById(R.id.readFile);
        fileWrite = (Button) findViewById(R.id.fileWrite);
        editText = (EditText) findViewById(R.id.editText);
        resultView = (TextView) findViewById(R.id.resultTextView);
        privateChk = (CheckBox) findViewById(R.id.chkExtrPrivate);
        publicChk = (CheckBox) findViewById(R.id.chkExtrPublic);
        sdcardChk = (CheckBox) findViewById(R.id.chkSDRoot);
        usageStorage = (CheckBox) findViewById(R.id.chkUsageStorage);
        aSwitch = (Switch) findViewById(R.id.switch1);

        fileRead.setOnClickListener(this);
        fileWrite.setOnClickListener(this);
        usageStorage.setOnCheckedChangeListener(this);


    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.fileWrite:
                Log.e(TAG,"onClick() : fileWiter");
                fileWrite();
                break;
            case R.id.readFile:
                Log.e(TAG,"onClick() : fileRead");
                fileRead();
                break;
        }
    }

    /**
     * After check box, select file use
     * @param
     * @return void
     */
    private void fileWrite(){
        Log.e(TAG,"onClick() : fileWiter()");
        if(aSwitch.isChecked()) {
            Log.e(TAG, "External");
            //외장
            if(checkedIntExterOnOff()){
                Log.e(TAG, "External on");
                if(isExternalStorageWritable()){
                    Log.e(TAG, "External Storage Writable ");
                    //external private checked
                    if(privateChk.isChecked()){
                        file = getExternalFilesDir(null); //external memory of private of app
                    }
                    //getExternalFilesDir(Environment.DIRECTORY_DCIM);
                    //external public checked
                    if(publicChk.isChecked()){

                    }

                    if(sdcardChk.isChecked()){
                        file = Environment.getExternalStorageDirectory(); // external
                    }
                    if(usageStorage.isChecked()){

                    }


                    Log.e(TAG, file.toString());
                }

            }
        }else{
            Log.e(TAG, "internal");
            //wirte App's directory in internal memory
            try{
                /*
                openFileOutput()을 호출하여 내부 디렉터리의 파일에 데이터를 쓰는
                FileOutputStream을 가져올 수도 있습니다.
                */
                FileOutputStream outputStream= openFileOutput(fileName, Context.MODE_PRIVATE);
                //outputStream.write(string.getBytes());
                outputStream.write(editText.getText().toString().getBytes());
                outputStream.close();
            }catch(Exception e){
                e.printStackTrace();
            }
        }

    }

    private void fileRead(){
        Log.e(TAG,"onClick() : fileRead()");
        if(checkedIntExterOnOff()){
            Log.e(TAG,"onClick() : fileRead() External");
        }else {
            try {
                Log.e(TAG, "filerRead internal");
                FileInputStream inputStream = openFileInput(fileName);
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder sb = new StringBuilder();

               String line ;
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line);
                }

                resultView.setText(sb.toString());
                resultView.setMovementMethod(new ScrollingMovementMethod()); //scroll
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * This function check Writable external memroy
     * @return
     * true : writable
     * false : no writable
     */
    private boolean isExternalStorageWritable(){
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)){
            return true;
        }

        return false;
    }

    /**
     * This function check readable external memory.
     * @return
     * true : readable
     * falsee : don't readable
     */
    private boolean isExternalStorageReadable(){
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)){
            return true;
        }
        return false;
    }


    /**
     * checked internal, external memory
     * @return
     * true : external
     * false : internal
     */
    private boolean checkedIntExterOnOff(){

        if(toggleButton.isChecked()){
            if(isExternalStorageReadable() || isExternalStorageWritable()){
                return true;
            }
        }

        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        deleteFile(fileName);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(usageStorage.isChecked()){
            Log.e(TAG,"Usage storage checked");

            if(privateChk.isChecked() || publicChk.isChecked() || sdcardChk.isChecked()){
                file = getExternalFilesDir(null);
                long freeSize = file.getFreeSpace();
                String strFreeSize = fileEx.sizeFormat(freeSize);
                long totalSize = file.getTotalSpace();
                String strTotalSize = fileEx.sizeFormat(totalSize);
                Log.e(TAG, "External Free memory size : " + strFreeSize);
                Log.e(TAG, "External total memory size : " + strTotalSize);
            }


        }else{
            Log.e(TAG, "Usage Storage uchecked");
        }

    }

    /**
     * formatting memory size
     * @param size
     * @return String
     */
    private String formatSize(long size){
        String suffix = null;
        if(size >= 1024){
            suffix = "kiB";
            size /= 1024;
            if(size >= 1024){
                suffix = "MiB";
                size /= 1024;
                if(size >= 1024){
                    suffix = "GiB";
                    size /= 1024;
                }
            }
        }
        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));
        int commaOffset = resultBuffer.length() -3;
        while(commaOffset > 0){
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if(suffix != null)
            resultBuffer.append(suffix);

        return resultBuffer.toString();
    }
}
