package com.pantech.firstfragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by user on 2017-03-14.
 */

public class ContentsFragment extends Fragment {

    final static String ARG_POSITION = "position";
    int mCurrentPosition = -1;
    TextView textView;
    LayoutInflater layoutInflater;
    View view;
    final String TAG = "Content_FRG";
    Button shardButton;
    Button sharedReadButon;
    final int RESULT = 1004;
    final int RESULTCODE = 100;
    final String SEND_INT = "result";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        Log.e(TAG, "onCreateView()");

        if(savedInstanceState != null){
            mCurrentPosition =  savedInstanceState.getInt(ARG_POSITION);
        }
        layoutInflater = inflater;
        view = layoutInflater.inflate(R.layout.content_view, container, false);

        shardButton = (Button) view.findViewById(R.id.sharedPref);
        shardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //SharedPreference 쓰기
                //SharedPreferences sharedPreferences = getActivity().getSharedPreferences("shared", Context.MODE_PRIVATE);
                SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("shared", 1);
                editor.commit();
            }
        });

        sharedReadButon = (Button) view.findViewById(R.id.sharedRead);
        sharedReadButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SharedPreference 읽기
                SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                //int shared = getResources().getInteger();
                int result = sharedPreferences.getInt("shared", 100);
                Toast.makeText(view.getContext(), "Shared Read : " + String.valueOf(result), Toast.LENGTH_LONG).show();
            }
        });
        //return super.onCreateView(inflater, container, savedInstanceState);

        Button sendActivity = (Button) view.findViewById(R.id.nextActivityBtn);
        sendActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewActivity.class);
                //intent.setAction("com.pantech.firstfragment.servicetest");
                intent.putExtra(SEND_INT, RESULT);
                intent.setType("text/plain");
                startActivityForResult(intent, RESULTCODE);

            }
        });
        return view;
    }

    public void upDateContentView(int position){
        Log.e(TAG, "upDateContentView(): position : " + position);
        textView = (TextView) getActivity().findViewById(R.id.textView);
        textView.setMovementMethod(new ScrollingMovementMethod()); //  scroll
        if(textView != null){
            textView.setText(Ipsum.Articles[position]);

            mCurrentPosition = position;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        Log.e(TAG, "onStart()");
        Bundle args = getArguments();
        if(args != null){
            upDateContentView(args.getInt(ARG_POSITION));
        }else if(mCurrentPosition != -1){
            upDateContentView(mCurrentPosition);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.e(TAG, "onSaveInstanceState()");
        outState.putInt(ARG_POSITION, mCurrentPosition);
    }



}
