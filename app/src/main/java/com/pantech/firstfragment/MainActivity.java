package com.pantech.firstfragment;

import android.app.FragmentTransaction;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends FragmentActivity implements HeadlineFragment.OnHeadlineSelectedListener {

    ContentsFragment contentsFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if((findViewById(R.id.headline_frg) != null) && (findViewById(R.id.contents_frg) != null)){
            if(savedInstanceState != null){
                return;
            }

            HeadlineFragment headlineFragment = new HeadlineFragment();
            contentsFragment = new ContentsFragment();
            headlineFragment.setArguments(getIntent().getExtras());
            contentsFragment.setArguments(getIntent().getExtras());

            getFragmentManager().beginTransaction()
                    .add(R.id.headline_frg, headlineFragment)
                    .add(R.id.contents_frg, contentsFragment)
                    .commit();

        }



    }

    @Override
    public void onArticleSelected(int position) {

        if(contentsFragment != null){
            contentsFragment.upDateContentView(position);
        } else{
            ContentsFragment content = new ContentsFragment();
            Bundle args = new Bundle();
            args.putInt(ContentsFragment.ARG_POSITION, position);
            content.setArguments(args);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.contents_frg, content);
            transaction.commit();
        }
    }
}
